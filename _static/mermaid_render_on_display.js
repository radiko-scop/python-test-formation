let render_on_display = function() {

    const main_section_nodes = document.querySelectorAll("div.slides > section");

    const observer_config = {
        attributes: true,
        attributesFilter: ['hidden'],
    };

    const callback = function (mutationList, observer) {
        mutation = mutationList[0]
                target = mutation.target;

                if (!target.classList.contains('present'))
                    return;

                mermaids = target.querySelectorAll(".mermaid");
                mermaids.forEach( (entries) => {
                    code = entries.textContent;

                    // Generate a unique-ish ID so we don't clobber existing graphs
                    // This is definitely quick and dirty and could be improved to
                    // avoid collisions when many charts are used
                    let id = 'graph-' + Math.floor(Math.random() * Math.floor(1000));

                    svg = mermaid.render(id, code);
                    entries.innerHTML = svg;
                })

                observer.disconnect()
    }

        main_section_nodes.forEach(function(n) {
            let observer = new MutationObserver(callback);
            observer.observe(n, observer_config);
        });
};

