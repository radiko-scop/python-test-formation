"""
module docstring looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong
"""

import datetime
import re


def mafonctionratee():
    """
    method docstring. On met du texte au format rst ...
    Example
    -------
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        x = mafonctionratee()
        assert x == None

    Notes
    -----
        This is an example of an indented section. It's like any other section,
        but the body is indented to help it stand out from surrounding text.

    """
    return re.search(datetime.datetime.now().isoformat(), 'ou')


def typo_example():
    """
    method docstring
    """
    for volume in [1, 2, 3]:
        print(volume)

    for volme in [4, 5, 6]:
        print(volume)

def foo():
    # McCabe rating is 11 here (by default 10)
    myint = 2
    if myint == 5:
        return myint
    elif myint == 6:
        return myint
    elif myint == 7:
        return myint
    elif myint == 8:
        return myint
    elif myint == 9:
        return myint
    elif myint == 10:
        if myint == 8:
            while True:
                return True
        elif myint == 8:
            with myint:
                return 8
    else:
        if myint == 2:
            return myint
        return myint
    return myint

def print_sum(param_a:int, param_b:int) -> str:
    return f"{param_a} + {param_b} = {param_a + param_b}"

my_sum:int = print_sum(1, 2.5)
