TP 3 : Outils pour améliorer la qualité du code
================================================

Prérequis (optionnel)
-----------------------

Si vous avez un IDE dédié à python (vscode, pycharm), vous pouvez regarder un tutoriel sur l'intégration d'un linter.
`Le lien pour vscode <https://code.visualstudio.com/docs/python/linting>`_.

Installez un analyseur de code python. Il en existe pas mal, parmi les populaires on citera pylint, flake8, mypy.
Chacun essaie de prédire des erreurs de code, et d'imposer des règles de codage.

Pour les installer, on fera un classique ``pip install mypy`` ou autre.

.. class:: info
    Ici on présente des outils python, mais le concepts existent quel que soit le language, et des équivalents existent.
    
Bases
-----------

.. class:: info

    Voir ce `site <https://vald-phoenix.github.io/pylint-errors/>`_ pour comprendre et approfondir les erreurs.

Creez un fichier python (`lab4.py`) par exemple.

.. code-block:: python

  def mafonctionratee()
    return ""

Notez, si votre IDE le permet la coloration syntaxique qui apparait quand vous sauvez.

Dans un terminal :

.. code-block:: bash

  python -m pylint lab4.py
  # ou
  python -m flake8 lab4.py
  # ou
  python -m mypy lab4.py

Corrigez l'erreur obtenue. On voit que, sans lancer notre code python, on a pu anticiper des erreurs d'exécution.
Voyons en certaines:

Lines too long
++++++++++++++++

Écrivez un commentaire ou une chaine de caractères de plus de 80 caractères : voyez le résultat. Est-ce toujours pertinent ?

Exemple : ``# A veryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy long comment``

Wrong import order
++++++++++++++++++++

.. code-block:: python

  """
  module docstring
  """

  import datetime
  import re

  import requests

  def mafonctionratee():
      """
      method docstring
      """
      return re.search(datetime.datetime.now().isoformat(), 'ou')


Typo
++++++

Ajoutez la fonction suivante :

.. code-block:: python

  def typo_example():
      """
      method docstring
      """
      for volume in [1, 2, 3]:
          print(volume)

      for volme in [4, 5, 6]:
          print(volume)

Regardez l'erreur obtenue avec pylint : ``python -m pylint lab4.py``. Pertinent ?


Complexité
++++++++++++

Testez la fonction suivante, issue de `ce github <https://vald-phoenix.github.io/pylint-errors/plerr/errors/design/R1260>`_:

.. code-block:: python

  def foo():
      # McCabe rating is 11 here (by default 10)
      myint = 2
      if myint == 5:
          return myint
      elif myint == 6:
          return myint
      elif myint == 7:
          return myint
      elif myint == 8:
          return myint
      elif myint == 9:
          return myint
      elif myint == 10:
          if myint == 8:
              while True:
                  return True
          elif myint == 8:
              with myint:
                  return 8
      else:
          if myint == 2:
              return myint
          return myint
      return myint

Tentez de corrigez les erreurs. Sont elles pertinentes ?

Typage statique
+++++++++++++++++

Python est un langage ou le type est dynamique. Cependant, pour certaines applications on peut vouloir forcer un typage statique.
On peut indiquer les types des objets dans les déclarations de fonction python, avec la syntaxe suivante :

.. code-block:: python

  def print_sum(param_a:int, param_b:int) -> str:
    return f"{param_a} + {param_b} = {param_a + param_b}"

  my_sum:int = print_sum(1, 2.5)

Copiez la fonction et son appel dans votre fichier python, puis lancez ``python -m mypy lab4.py``.
Expliquez le résultat. Pour information, vous devriez avoir quelque chose ressemblant au message ci-dessous:

.. code-block:: bash

  lab4.py:55: error: Incompatible types in assignment (expression has type "str", variable has type "int")
  lab4.py:55: error: Argument 2 to "print_sum" has incompatible type "float"; expected "int"
  Found 2 errors in 1 file (checked 1 source file)

Qu'en pensez vous ?

Automatiquement reformater son code avec black
---------------------------------------------------

Certains outils permettent de reformater son code pour s'approcher des standards, comme `black <https://pypi.org/project/black/>`_.

Testez les commandes suivantes :

.. code-block::

  python -m black --check --target-version=py39 lab4.py # liste les fichiers à modifier
  python -m black --diff --target-version=py39 lab4.py # affiche le diff des modifs prévues
  python -m black --target-version=py39 lab4.py # effectue les modifs

Qu'en pensez vous ?

**Note** : cette opération peut être automatisée avant de commit, tout comme celles liées au linters.

Je n'entrerai pas plus dans le détail ici.

Analyse statique sur un module existant
-----------------------------------------

Depuis le repository utilisé lors des TPs précédents, lancez la commande ``python -m pylint src/``

Relevez la note obtenue.

Quelles sont les principales reproches que pylint fait aux auteurs du code ?

Qu'en pensez vous ?

Est-il possible d'ignorer certaines erreurs pour avoir un meilleur score ? Est-ce que cela vous parait pertinent ?

.. class:: info

    ``pylint --reports=y`` peut vous donner des informations supplémentaires intéressantes.

Documentation
----------------

Un code de qualité va avec une documentation de qualité.
Des outils permettent de générer une documentation propre rapidement, tout en gardant la documentation au plus près du code.

On va générer une documentation pour le code écrit dans le fichier lab3.py

.. code-block:: bash

  pip install sphinx
  sphinx-quickstart

  # sous linux
  make html

  # sous windows
  cd source
  sphinx-build.exe . build -b html

Ouvrez build/html/index.html. Cela vous évoque quelque chose ? (`readthedoc par exemple ? <https://readthedocs.org/>`_)

Ajouter la ligne ``'sphinx.ext.autodoc',`` dans les extensions de ``source/conf.py``.

Décommentez les lignes suivantes :

.. code-block:: python

  # -- Path setup --------------------------------------------------------------

  # If extensions (or modules to document with autodoc) are in another directory,
  # add these directories to sys.path here. If the directory is relative to the
  # documentation root, use os.path.abspath to make it absolute, like shown here.
  #
  import os
  import sys
  sys.path.insert(0, os.path.abspath('..'))

pour que sphinx trouve votre fichier lab3.py dans son path.

.. class:: info

    Le chemin donné est relatif au dossier source.

Lancer ``sphinx-autodoc`` puis ``make html``. Il y a une erreur, car les fichiers générés avec la commande précédente n'ont pas été ajouté à l'index (index.rst qui devient index.html). Pour le faire, on ajoute le nom du fichier qu'on veut ajouter dans index.rst:

.. code-block:: python

  Welcome to lab3's documentation!
  ================================

  .. toctree::
    :maxdepth: 2
    :caption: Contents:

    modules

Détaillez un peu la docstring associée aux fonctions du module, puis régénérez votre html. On pourra s'inspirer des exemples `sphinx <https://www.sphinx-doc.org/en/master/usage/extensions/example_numpy.html>`_.

Imposer des règles
-------------------

Parfois on peut vouloir imposer des règles en équipe, ou à sois même, pour garantir une certaine cohérence.
C'est possible relativement facilement gâce à `pre-commit <https://pre-commit.com/>`_.

Code review
--------------

Allez voir une merge request en cours de pull sur github, par exemple en explorant les projets ou en regardant les projets à la mode.

Par exemple: `la librairie Gtk <https://gitlab.gnome.org/GNOME/gtk/-/merge_requests>`_ utilise largement ce mécanisme pour son développement.

Qu'en pensez vous ? Voyez vous des avantages / des inconvénients ?

Outils de tracabilité des spécifications
------------------------------------------

On ne va pas l'aborder dans cet atelier, mais sachez qu'en fonction de vos besoins normatifs il existe des outils pour vérifier que toutes vos spécifications sont bien couverte, et testées. On peut citer les suivants:

  - `Jira <https://www.atlassian.com/software/jira>`_ ( application de tickets, liée à des workflows )
  - `Tuleap <https://www.tuleap.org/product/tuleap-enterprise-edition-features/>`_ ( même genre que Jira, mais fait aussi de la gestion documentaire, et possède un module pour le médical )
  - `Doors <https://www.ibm.com/products/requirements-management>`_ ( référence, que je ne connais pas bien )
  - `Reqtify <https://www.3ds.com/products-services/catia/products/reqtify/>`_ ( fait par Dassault. Lie des documents word ou autre entre eux. )

Quelques liens
---------------

- https://learn.adafruit.com/improve-your-code-with-pylint?view=all
- https://pythonspeed.com/articles/pylint/
- https://betterprogramming.pub/auto-documenting-a-python-project-using-sphinx-8878f9ddc6e9
