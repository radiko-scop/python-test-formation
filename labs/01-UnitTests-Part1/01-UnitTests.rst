TP1 : Tests unitaires
=========================

Objectifs
-----------

Améliorer la qualité d'un module python disponible sur le net, permettant de supprimer les doublons d'un fichier csv.

Il existe de nombreux frameworks de tests sous python.
Je vous propose de commencer avec pytest.

Documentation : https://docs.pytest.org

.. class:: info

Ici on présente des outils python, mais le concepts existent quel que soit le language, et des équivalents existent.
    
.. class:: info

Dans la suite du TP, on utilise pip comme package manager, et le module natif de python venv pour l'environnement virtuel. Vous êtes bien entendu libres de faire autrement si vous avez vos habitudes.

Premiers pas : mise en place d'un environnement de développement
-----------------------------------------------------------------------------

Pour développer, vous aurez besoin:

- D'un éditeur de texte ou IDE (je conseille vscode pour ce tp)
- D'une version de python avec pip installée et fonctionnelle **ou** d'une version de docker installée (idéal)

Commencez par cloner le projet de base:

.. code-block:: bash

    git clone https://gitlab.com/radiko-scop/csv-cleaner.git
    # ou git clone git@gitlab.com:radiko-scop/csv-cleaner.git si vous avez un compte gitlab configuré en ssh
    cd csv-cleaner

Choisissez l'une des options en fonction de ce dont vous avez l'habitude.

.. class::info

    Regardez le warning, faite setx PATH "%PATH%;<path annoncé>"

Option 1 : vscode + devcontainer
+++++++++++++++++++++++++++++++++

Ouvrez le projet que vous avez cloné précédemment avec vscode et acceptez de démarrer dans le devcontainer.

Option 2 : docker + bash
+++++++++++++++++++++++++++++++++

Ouvrez un terminal dans le dossier que vous venez de cloner.
Exécutez la commande : ``docker run -v `pwd`:/app:z -it --entrypoint /bin/bash --name csv_cleaner_dev --network host python``
Puis une fois dans le container : ``cd /app && pip install -e .``. Cette commande permet d'avoir le module en cours de développement dans le path utilisé par python pour chercher les modules, et permet d'importer notre module en cours de développement dans python.

Option 3 : mise en place manuelle du projet
+++++++++++++++++++++++++++++++++++++++++++++

.. class:: info
    Certaines choses ne fonctionnent pas de manière répétable sous windows, je conseille si vous en avez la possibilité d'utiliser une VM avec un linux.
    
Pour travailler correctement et ne pas polluer votre système, il est conseillé de faire un environnement virtuel pour python, de la manière suivante:

.. code-block:: bash

    python -m venv .venv
    source .venv/bin/activate #  .venv/Scripts/Activate sous windows

Vous pourrez alors installer les dépendances du package et l'installer dans le path python pour edition :

.. code-block:: bash

    pip install -e .

FAQ
+++++++

Impossible d'installer le soft avec pip install

.. code-block:: python

    import sys, os
    sys.path.append(os.getcwd()+"/src")
    
Attention, on perd les linter et autre quand on fait ca, c'est pas conseillé.

Premiers pas : test vide
--------------------------

.. code-block:: bash

    mkdir test
    touch test/test_assert.py # ou bien créér manuellement le fichier test_assert.py

Ouvrir test_assert.py et écrire :

.. code-block:: python

    def test_assert_true():
        assert True

    def test_assert_false():
        assert False

Puis éxecuter:

.. code-block:: bash

    pip install pytest
    python -m pytest
    echo $?

- Quelle est la valeur de retour affichée ?
- Faire en sorte que le test qui échoue soit marqué comme "SKIPPED" (voir la `documentation <https://docs.pytest.org/en/6.2.x/skipping.html?highlight=skip>`_).
- Relancer les tests et observer le résultat.

{{CORR


Correction
++++++++++++++

La valeur de retour affichée est 1, ce qui veut dire qu'on a une erreur. Si on modifie le test comme ci-dessous, le code erreur vaudra 0, ce qui veut dire que pytest considère le résultat des tests comme un succès.

.. code-block:: python

    @pytest.mark.skip(reason="always fail")
    def test_assert_false():
        assert False


CORR}}

Test basique
-------------------------------

Importez les modules cleaner et utils du package csv_cleaner pour pouvoir le tester:

.. code-block:: python

    from csv_cleaner import cleaner, utils

Puis écrivez des tests simples pour tester les fonctions ou attributs suivants du module cleaner:

    - ``cleaner.__version__`` (renvoie la version du packet que vous testez)
    - ``utils.get_csv_sample_100()`` (renvoie un exemple de csv de 100 ligne)
    - ``utils.get_csv_sample_10000()`` (renvoie un exemple de csv de 10000 ligne)
    - ``utils.concat(string1, string2)`` (concatène les deux strings)

- Notez la dernière ligne qui s'affiche dans le rapport de pytest (dans mon cas : 6 passed in ~1s).
- Dans l'hypothèse ou l'on a des dizaines, voire des centaines de tests, est-ce que ça vous parait admissible ?

- On peut exécuter les tests en parallèle. Donner la commande pour le faire (à trouver dans la doc ou dans ce `tutoriel <https://qxf2.com/blog/xdist-run-tests-parallel-pytest/>`_), et l'essayer. On pourra aussi regarder le plugin `pytest-xdist <https://pypi.org/project/pytest-xdist/>`_.

Gagne-t-on du temps ? assez ?

{{CORR


Correction
++++++++++++++

Certaines fonctions sont assez lentes dans les tests puisque ils mettent plus d'une seconde à s'exécuter. Si on a des centaines voire des milliers de tests, l'exécution prendra trop de temps, et on sera tenté de ne pas exécuter les tests.

L'exécution en parallèle peut aider dans certains cas, mais souvent il faudra soit modifier son test, soit trouver et supprimer les cause des délais (ici, un accès à internet dont on pourrait se passer).

CORR}}


Vérifier les exceptions
-------------------------------------------

Lancer le test suivant :

.. code-block:: python

    def test_clean_csv_file_not_found():
        cleaner.clean_csv("unexistantFile")

- Est-il normal qu'il échoue ?
- Modifiez le pour qu'il passe, sans changer la ligne ``cleaner.clean_csv("unexistantFile")`` ni le marquer comme xfail, ni créer le fichier attendu. (on pourra s'aider de `la documentation <https://docs.pytest.org/en/6.2.x/assert.html>`_).


{{CORR

Correction
++++++++++++++

Le code ci-dessous indique à pytest qu'on cherche à vérifier la levée d'une exception.
On peut catcher l'erreur si besoin, pour vérifier son contenu exact (message, ...), mais ici on ne vérifie que son type.

.. code-block:: python

    def test_clean_csv():
        with pytest.raises(FileNotFoundError):
            cleaner.clean_csv("toto")

CORR}}

Fixture
-------------------------------------------

Une fixture ressemble à ceci :

.. code-block:: python

    import pytest

    @pytest.fixture
    def csv_sample():
        return """Sexe,Prénom,Année de naissance
    M,Alphonse,1932
    F,Béatrice,1964
    F,Charlotte,1988""" # ou n'importe quel example de csv

- Faite passer un test testant la fonction ``utils.parse_csv()`` en utilisant cette fixture (aidez vous de la `documentation des fixtures <https://docs.pytest.org/en/6.2.x/fixture.html>`_).
- A quoi sert une fixture ?
- Dans ce cas, la fixture proposée est elle pertinente ?
- Proposez un cas dans lequel une fixture prend tous son sens (inutile de l'implémenter).

{{CORR

Correction
++++++++++++++

Le code ci-dessous montre l'utilisation d'une fixture.

.. code-block:: python

    @pytest.fixture
    def csv_sample():
        return """Sexe,Prénom,Année de naissance
    M,Alphonse,1932
    F,Béatrice,1964
    F,Charlotte,1988"""


    def test_parse_csv(csv_sample):
        res = utils.parse_csv(csv_sample)
        assert ["M", "Alphonse", "1932"] in res


Ici la fixture n'a pas grand intérêt. Ça devient intéressant quand on veut partager facilement une manière de construire un objet complexe entre de nombreux tests (ex : une connexion à une base de donnée, ...). Ca aide aussi à définir des prérequis pour nos tests. On pourrait utiliser des fonction, ou des variables globales, mais ce serait plus compliqué.

CORR}}

Utilisation du debugger
--------------------------

Dans la mesure ou les test échouent souvent, il peut être intéressant pour mieux comprendre pourquoi de lancer un debugger en cas d'échec. Cela se fait simplement avec pytest via la commande :

.. code-block:: bash

    python -m pytest --pdb

.. class:: info

    L'ajout d'un appel à la fonction **breakpoint()** n'importe ou dans le code lance aussi un debugger à cet endroit, indépendamment des tests.

Je vous renvoie vers la `documentation de pdb <https://docs.python.org/3/library/pdb.html>`_ pour les détails d'utilisation.

Mettez un breakpoint dans le fichier utils.py, fonction get_csv_sample, au niveau du return (l145):

.. code-block:: python

    def get_csv_sample(url):
        res = requests.get(url)
        breakpoint()
        return parse_csv(res.text)

Lancez un test et essayez d'afficher des informations sur ``res``.

- Quel est sa classe ?
- Utilise-t-on toute cette classe, ou bien une partie seulement ? Quelle forme a l'élément retourné par la fonction dans laquelle vous avez mis votre breakpoint ?

{{CORR

Correction
++++++++++++++

Taper res dans l'invite de commande de gdb nous donne sa classe et son code (``<Response [200]>``). La fonction ``dir(res)`` nous donne la liste des méthodes et attributs de res.

On voit qu'on retourne ``res.text``. Si on regarde sa valeur, ça semble être une chaine de caractères. ``res.text.__class__`` nous le confirme.

On voit que seule cette chaine de caractères est utilisée dans la suite. 

CORR}}

Utilisation du debugger avec vscode
----------------------------------------

Je vous laisse lire la `doc de vscode <https://code.visualstudio.com/docs/python/debugging>`_
Refaite l'étape précédente avec vscode (Ou votre IDE).

Qu'est ce que l'utilisation de l'IDE vous apporte ?

{{CORR

Correction
++++++++++++++

L'ide nous apporte plus de simplcité, on peut notamment explorer facilement la pile d'appel pour savoir qui a appelé quelle fonction, et les variables accessibles depuis la fonction.

CORR}}

Intégration de Pytest avec vscode
-----------------------------------

https://code.visualstudio.com/docs/python/testing

Cliquez sur l'icône testing à gauche (en forme d'Erlenmeyers).
Vous devriez avoir la liste de vos tests si vous suivez correctement la doc.

Paramétrer un test
-------------------------------------------

Testez la fonction ``utils.concat`` avec au moins trois couples de paramètres différents (pourquoi pas également des types différents ?), en modifiant juste les décorateurs et l'entête d'une fonction test_concat qui ressemblerait à ça :

.. code-block:: python

    def test_concat():
        assert utils.concat("toto", "tata") == "tototata"

Vous pourrez utiliser la `documentation des paramètres de tests <https://docs.pytest.org/en/6.2.x/parametrize.html#parametrize-basics>`_).

On aura alors à l'éxécution :

.. code-block:: python

    python -m pytest -k test_concat

.. class:: info
    L'option -k sert à selectionner un test (ou des tests si ils sont plusieurs à avoir une partie de leur nom en commun)

Vous devriez avoir un résultat du type :

.. code-block:: bash

    root@7b134f0bfbdc:/workspaces/csv-cleaner# python -m pytest -k test_concat
    ============================================================================== test session starts ===============================================================================
    platform linux -- Python 3.11.4, pytest-7.4.0, pluggy-1.2.0
    rootdir: /workspaces/csv-cleaner
    collected 7 items / 4 deselected / 3 selected                                                                                                                                    

    test/first_test.py ...                                                                                                                                                     [100%]

    ======================================================================== 3 passed, 4 deselected in 0.21s =========================================================================

Pourquoi est-il indiqué que 3 tests ont passé, alors qu'on en a exécuté qu'un seul ?

{{CORR

Correction
++++++++++++++

La paramétrisation permet de rejouer le même test avec des jeux de donnes différents en entrée. Ici on avait 3 jeux de données, donc le test a été exécuté trois fois.

CORR}}


Mock ou bouchon
-----------------------------------------------------------

Le module utils utilise le package requests.
Je propose de se substituer à ce package (qui n'est pas l'objet du test), pour :

- Gagner du temps en retournant une page stockée en locale plutôt qu'à distance
- Générer facilement des erreurs lors de la requête

L'opération de substitution s'appelle un mock en anglais, ou un bouchon en francais.
Sachant que le seul appel effectué par le module utils à la librairie requests est le suivant :

.. code-block:: python

       res = requests.get(url)

et que la `documentation des mock <https://docs.pytest.org/en/6.2.x/monkeypatch.html?highlight=mock>`_ de pytest explique justement comment mocker une requète, utilisez cette possibilité pour mocker le test de la fonction ``utils.get_csv_sample_10000()`` (ne faire que des appels à pytest au lieu des appels à requests). On pourra charger un fichier par exemple plutôt qu'aller chercher les données sur le web. Si ouvrir un fichier est compliqué, on pourra aussi générer 10000 fois la même ligne (exemple de code : ``"my, first, row\n"*10000``).

- Comparez le temps d'exécution de ce test avant et après le mock.
- Gagne-t-on un temps significatif ?
- Quels sont les autres avantages du mock ?

.. class:: info
    **Rappel** la commande ``pytest -k <nom du test>`` permet de n'exécuter qu'un seul test.

Ajoutez un test ou requests renvoie un timeout, ou une erreur 404 par exemple. Proposez une modification du code pour que le timeout soit pris en compte.

{{CORR

Correction
++++++++++++++

- On gagne clairement du temps, puisqu'on ne dépend plus d'internet.
- On peut facilement tester toutes les réacitons attendues 

Exemple d'utilisation:

.. code-block:: python

    # custom class to be the mock return value
    # will override the requests.Response returned from requests.get
    class MockResponse:

        def __init__(self, text):
            self.text = text


    def test_get_json(monkeypatch, csv_sample):

        # Any arguments may be passed and mock_get() will always return our
        # mocked object, which only has the .json() method.
        def mock_get(*args, **kwargs):
            return MockResponse(csv_sample)

        # apply the monkeypatch for requests.get to mock_get
        monkeypatch.setattr(requests, "get", mock_get)
        res = utils.get_csv_sample_10000()
        assert len(res) == 4

CORR}}

Pistes & rappels utiles
-----------------------

``python -m pytest -k <nom du test ou partie du nom>`` pour lancer uniquement certains tests
``python -m pytest -s`` pour avoir la sortie standard (voir les print)
``python -m pytest --pdb`` pour lancer le debugger au premier test qui échoue (cf aide de pdb pour plus d'infos)


