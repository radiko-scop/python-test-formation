TP2 : Tests unitaires - suite
=================================

Objectifs
-----------

On repart du projet précédent, permettant de nettoyer un csv.

.. class:: info

    Ici on présente des outils python, mais le concepts existent quel que soit le language, et des équivalents existent.


.. class:: info

    **N'oubliez pas de sourcer votre environnement virtuel si vous n'utilisez pas docker !**

Faites échouer un ou deux tests artificiellement, afin de voir des sorties positives et négatives.

Gestion des sorties de pytest
------------------------------------

La documentation se trouve `ici <https://docs.pytest.org/en/latest/how-to/output.html>`_.

Lancer ``python -m pytest --junitxml=report.xml``

Ouvrez le fichier résultat. Retrouvez vous les concepts que vous attendiez ?

{{CORR

Correction
++++++++++++++

On observe un XML standardisé, qui peut être utilisé par exemple par un outil de monitoring standardisé.
Le standard est, comme son nom l'indique, junit. On y retrouve bien des notions habituelles comme celle de testcase.

CORR}}

Personnaliser la sortie
------------------------

Pytest permet de s'interfacer facilement pour générer des sorties sur mesure ou effectuer des actions.

On peut donc créer facilement son rapport. Nous n'allons pas renter dans le détail ici, mais simplement utiliser un module d'extension à pytest qui se sert des ces mécanismes pour générer du html.

Installer pytest-html (``pip install pytest-html``)

Lancer ``python -m pytest --html=report.html --self-contained-html``

Regardez le rapport obtenu. Voyez vous des cas ou c'est plus intéressant que d'afficher la console ?

{{CORR

Correction
++++++++++++++

Le html est plus intéressant à garder si on veut l'afficher directement, ou le stocker en tant que rapport associé à une version.

CORR}}

Couverture de code
--------------------

On va maintenant observer la couverture de code obtenue via vos tests.
Cet indicateur vous permet de savoir quelle partie de votre code a été exécutée lors des tests.

Pour cela, on installe une nouvelle extension de pytest: ``pip install pytest-cov``

Puis on lance ``python -m pytest --cov``.
Observez la sortie standard. Qu'en pensez vous ? Le coverage est-il satisfaisant ?
On observe que vous avez le dossier de test dans les statistiques. Cherchez l'option de coverage permettant de n'étudier que le coverage du module csv_cleaner.

Lancez maintenant ``python -m pytest --cov --cov-report html``.
Ouvrez ensuite ``htmlcov/index.html`` dans un navigateur, et lisez un peu le rapport.
Qu'en pensez vous ? Doit on imposer une couverture à 100% ? Y a-t-il des chose à tester en priorité ?

Si vous deviez ajouter un test, que testerai-t-il ? Pourquoi ?


.. class:: info

    Pensez à la place des tests unitaires dans la phase de vérification et validation.

{{CORR

Correction
++++++++++++++

Lorsqu'on lance le coverage avec la commande ``pytest --cov=csv_cleaner``, on observe :

.. code-block:: bash

    ---------- coverage: platform linux, python 3.11.4-final-0 -----------
    Name                          Stmts   Miss  Cover
    -------------------------------------------------
    src/csv_cleaner/__init__.py       6      0   100%
    src/csv_cleaner/cleaner.py       81     52    36%
    src/csv_cleaner/utils.py         13      1    92%
    -------------------------------------------------
    TOTAL                           100     53    47%
    
La lecture du rapport html nous montre le code non testé, notamment dans ``cleaner.py`` qui a un faible coverage.

**Important** Il faut garder à l'esprit qu'on ne peut pas tester un programme à 100% (par exemple, si on multiplie deux entiers, on ne va pas tester tous les couples d'entiers possibles). Faire la course au pourcentage de coverage est souvent contre-productif, parce qu'on va privilégier la quantité de tests sur la qualité (Par exemple, si on teste un division, il vaut mieux faire un test qui vérifie ce qui se passe avec 0 que 10 tests qui regardent des valeurs au hasard).

Il faut aussi garder à l'esprit que d'autres tests sont probablement effectués sur le logiciel dans son ensemble. Donc les cas "nominaux" ne sont pas forcément très pertinent à tester non plus, puisqu'ils sont aussi testé lors des tests de plus haut niveau.   

CORR}}

Branches
---------

Créez le fichier test_branches.py dans le dossier test.
Il contiendra le code suivant :

.. code-block:: python

    def branches_are_tricky(a, b):

        if a or b :
            return a + b
        return 0

    def my_partial_fn(x):

        if x:
            y = 10
        return y

    def test_branches_are_tricky():
        assert branches_are_tricky(1, 2) == 3
        assert branches_are_tricky(None, None) == 0
        assert my_partial_fn(1) == 10

Regardez le code coverage de la fonction branches_are_tricky. Est il suffisant ? Si non, pourquoi ?

Ajoutez maintenant les asserts suivants à la fonction ``test_branches``:

.. code-block:: python

    assert branches_are_tricky(None, 2) == 2
    assert my_partial_fn(False) == 10

Pourquoi le test échoue-t-il ?

Cette subtilité est appelée une branche. Malgré une excellente couverture, on laisse passer des combinaisons génératrice d'erreur.

Certaines librairies de test sont capables d'indiquer si les branches ont été couvertes correctement (cf la `documentation <https://coverage.readthedocs.io/en/6.2/branch.html#branch>`_).

Pour que les branches soient analysées, vous devrez créer un fichier nommé ``.coveragerc`` à la racine du projet, contenant les lignes suivantes:

.. code-block:: python

    [run]
    branch = True

Relancer le coverage.
Est ce que toutes les branches ont été correctement détectées ?


{{CORR

Correction
++++++++++++++

Lorsqu'on lance le coverage, on voit ``test/test_branches.py            12      0   100%``. On pourrait donc supposer que toutes les fonctions sont bien testées.
Pourtant, quand on ajoute des assert simples, on voit rapidement qu'il reste plein de tests possibles, associés à des erreurs potentielles !



CORR}}

Le fichier `conftest.py <https://docs.pytest.org/en/6.2.x/writing_plugins.html#conftest-py-local-per-directory-plugins>`_
--------------------------------------------------------------------------------------------------------------------------------

Pour information, on peut configurer le comportement de pytest avec un fichier de configuration, ou des plugins.
Cela permet entre autres de définir des fixtures partagées entre les tests, un comportement de setup et teardown, des mocks ...

On peut aussi ajouter des paramètres d'entrée (imaginons qu'on aie taggé les tests trop lents, on peut lancer uniquement les tests rapides pour ne pas perdre de temps en local, et laisser un serveur distant s'occuper des tests plus longs.)

Je n'en parlerai pas plus faute de temps. Ce n'est pas indispensable pour débuter, et vous tomberez dessus lorsque vous demanderez conseil à votre moteur de recherche ou ia préféré.

Un bon résumé des utilisations possibles existe sur `stack overflow <https://stackoverflow.com/questions/34466027/in-pytest-what-is-the-use-of-conftest-py-files>`_.

Tester différentes versions de cible
----------------------------------------

Dans un certains nombres de cas, les tests sont dépendants de la cible. Un exemple simple est le système d'exploitation Linux/Windows/MacOs, mais on peut imaginer un grand nombre d'autre options (interface ethernet, architecture CPU, carte graphique ...).

Je ne vais pas rentrer dans le détail des outils permettant de faire le tri, mais sachez qu'il existe au moins un marqueur ``skipif`` qui peut permettre de skipper certains tests en fonction de la configuration.


Porter les tests chez quelqu'un d'autre
----------------------------------------

Comment s'assurer que les tests sont toujours exécuté pareil ? Que les bons packages seront installés si on a des dépendances ?

Des outils existent pour cela !

Si on ne veut pas y passer, on peut au moins écrire une petite procédure avec les différentes étapes pour lancer les tests.
La commande ``pip freeze`` permet de savoir quels paquets sont installés dans votre environnement, et dans quelle version.
On notera notamment la version de python utilisée.

Un outil populaire pour gérer ce genre de problématiques s'appelle `tox <https://tox.wiki/en/latest/>`_.
Il se définit comme "tox is a generic virtualenv management and test command line tool". En gros il automatise
les tâches que vous avez fait en début d'Atelier (installation d'un environnement virtuel, des dépendances, ...).
Il va même plus loin puisqu'il permet de lancer les tests tour à tour dans deux environnement différents (par exemple python 3.8 et python 3.10)

``pip install tox``
``tox -e py38``

Organisation des tests dans un projet python
------------------------------------------------

Les projets pythons répondent généralement à une arborescence organisée (voir par exemple https://packaging.python.org/en/latest/tutorials/packaging-projects/).
Un certain nombre d'outils permettent de générer cette arborescence.

Je vous propose d'étudier `pyscaffold <https://pypi.org/project/PyScaffold/>`_ afin de voir à quoi ça peut servir.

Créez un nouveau répertoire à la racine de votre TP, dans lequel exécuter cette étape.

.. code-block:: bash

    pip install pyscaffold
    putup my_project
    cd my_project
    git init
    tox

Qu'observez-vous ? Cette méthode pour créer un projet vous semble-t-elle pertinente ?

**note**: comme tout générateur, pyscaffold a ses désavantages, et en cas de problèmes il est plus difficile d'en comprendre la source.

**note2**: PyScaffold fait plus que vous générer une base de tests : il prépare la distribution de votre package python.

Essayez la commande : ``python setup.py --version``. Vous voyez que votre package a une version générée automatiquement. Pratique quand on est pas rigoureux sur ce point.

Vous pouvez aussi tenter la commande : ``tox -e docs`` et jeter un oeil au résultat (ie le fichier docs/_build/index.html). Aucun rapport avec les tests bien sûr, mais pratique.


**note3**: on peut aussi citer comme alternative à pyscaffold `cookiecutter <https://cookiecutter.readthedocs.io/en/1.7.2/>`_, et il en existe certainement un paquet d'autre. Pyscaffold propose même une extension pour cookiecutter.

Autres frameworks de test (toujours en python) & extensions
-------------------------------------------------------------

Je ne vais pas entrer dans le détail, et ne propose pas de faire de manipulation avec ces frameworks, mais voici quelques frameworks qui pourraient vous être utile.

`Unittest <https://docs.python.org/3/library/unittest.html>`_
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Le framework inclut dans python. Moins à mon goût que pytest, plus orienté objet... je vous laisse essayer !

`Django https://docs.djangoproject.com/fr/3.2/topics/testing/`_
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Django (le framework de référence pour faire des projets web en python) vient avec son frameworks de test, qui semble pertinent pour ce qui est de tester des sites générés avec Django. Il est basé sur unittest.

`doctest <https://docs.python.org/3/library/doctest.html>`_
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

On écrit les tests dans la documentation de la fonction.
Exemple :

.. code-block:: python

    """
    This is the "example" module.

    The example module supplies one function, factorial().  For example,

    >>> factorial(5)
    120
    """

    def factorial(n);
        ...

Je ne suis pas entièrement convaincu, mais peut être cela peut permettre de faire tourner les exemples de la documentation.
