TP4 : Tests de charge
=========================

Une très rapide introduction aux tests de charge.

Lancer votre projet portofolio (ou clonez le depuis https://github.com/radiko-scop/DjangoPortFolio.git)
Le lancer en local après avoir installé les requirements, (``python manage.py runserver``).

Un outil connu : JMeter
-------------------------

- Installez `Jmeter <https://jmeter.apache.org/download_jmeter.cgi>`_
- Lancez l'outil téléchargé
- Crééz un nouveau projet depuis le template "Web" (``File/Templates`` puis ``Building a Web Test Plan`` dans le menu déroulant)
- Un projet est alors créé.
- Cliquez sur ``Scenario 1/HTTP Request Defaults``
- Renseignez l'ip ``127.0.0.1`` et le Port number ``8000``
- Cliquez sur le bouton play en dans la barre en haut.

Est ce que le test passe ?

Modifiez ``Home Page/Assertion`` pour qu'il verifie le contenu de la page affichée par votre serveur Django.
Lancez le test, puis regardez dans ``View Results Tree``.

Enregistrez votre projet, puis fermez jmeter.

Lancez ``bin/jmeter -n -t <path/to/jmx/file.jmx> -l report.jtl`` pour effectuer le test sans IHM (on n'est pas supposés lancer les tests depuis l'IHM, c'est juste pour la configuration et le debug).

Pour afficher correctement le rapport jtl généré: ``bin/jmeter -g report.jtl report`` puis ouvrir ``report/index.html``.

`Cet article <https://www.techaheadcorp.com/blog/jmeter-testing/>`_ semble détaille un peu plus une procédure similaire.

Conclusion
+++++++++++++

Jmeter est assez puissant mais aussi assez lourd à configurer.

Autres outils
-------------------------

Pour les adeptes d'outils plus simple en ligne de commande (plus compliqués à installer), on pourra mentionner les outils suivants, utilisés professionnelement:

- wg/wrk
- h2load
- tsung
- resperf (performance dns)
