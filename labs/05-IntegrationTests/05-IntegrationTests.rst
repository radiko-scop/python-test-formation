TP 5 : Tests d'intégration
===========================


Objectifs
-----------

Une fois que votre programme, ou votre librairie est prête à être utilisée, il est possible que vous vouliez tester son comportement comme un boite noire, par exemple pour vérifier que ce que vous avez écrit dans le manuel (qu'on peut voir comme une spécification) est bien conforme à ce que votre programme fait.

Je vous propose d'étudier succinctement quelques outils en rapport avec des tests de plus haut niveau.


Behavior driven developpement / test automatisé haut niveau
-------------------------------------------------------------

L'idée est globalement la même qu'avec pytest, mais avec une écriture simplifiée des tests, qui doivent être lisible par des gens moins technique. 

- https://cucumber.io/
- https://robotframework.org/


Tests d'API
---------------

https://swagger.io/

à explorer:
http://www.fitnesse.org/
Steam by github
Jasmine
Protactor
