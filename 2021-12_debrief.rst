Retour d'expérience sur cette formation
========================================

Il s'agissait d'une formation en 14h30 d'élèves en B3 (bac + 3)

- Beaucoup trop court comme temps
- Niveau en python insuffisant au vu du programme proposé

28 élève c'est trop pour demander des notes individuelles sur ce temps. Je n'ai pas eu assez de temps par personne pour les suivre.

Il aurait mieux valu les mettre en groupe direct, individuellement ils n'avancent pas.

Certains ont perdu du temps (énormément !) sur leur environnement technique.

Autonomie très faible pour certains.

Aucunes connaissances en shell.

Il faudrait tout détailler, et envoyer un message avant pour qu'ils aient un env de dev.

Pas de background en info pour certains.


A faire plus : démo en situation:
----------------------------------

- ex : Parcourir la doc avec nous
- + de live coding / tutos youtube environ 1/3 du temps.