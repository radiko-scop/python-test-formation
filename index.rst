.. include:: <isotech.txt>
.. highlight:: python

.. Pour la présentation, https://obsproject.com/wiki/install-instructions#linux peut servir.

=====
Tests
=====

Tester un Logiciel

.. TOC
.. =======

.. .. contents::

Introduction
========================

Présentation
-------------

- Benjamin Forest
- Radiko
- benjamin.forest@radiko.fr
- Ces slides sont disponibles sur gitlab : https://gitlab.com/radiko-scop/python-test-formation

.. Présentation de moi et radiko. Expériences autour du test.
   Premiere formation pour moi. Soyez acteurs.
   Faire l'appel

Discussion sur votre cycle de développement logiciel
-----------------------------------------------------

- Comment savez vous ce qui doit être fait ?
- Comment vous organisez vous pour le faire ?
- Comment prouvez vous que cela a été fait correctement ? 

Au programme
-------------

.. image:: _static/v_cycle.svg
   :align: center

.. NB : ca reste valable quelque soit la méthode de dev.
   Plan de test
   Tests unitaires et liés
   Test de charge (bonus)
   20h c'est court. Je ne vais pas vous emm*** avec des définitions à gogo.
   Pour les gens qui voudraient plus d'infos, voir la biblio en fin de cours.

Livrable & Notation
--------------------

- Je n'ai aucune info
- Possibilité de faire les TPs à 2
- Notes individuelles.
- Livrable : 1 zip par **personne**:

  * Code source des tests écrits.
  * Compte rendu des TPs (simple). *Au format PDF*.
- à envoyer à benjamin.forest@radiko.fr
- Date inconnue

..  Je ne vais pas corriger, juste regarder et mettre une note -> être concis.

Pourquoi tester
------------------------

Établir la qualité, la performance ou la fiabilité.

.. Haute fiabilité
  Normes (Médicales type EN-62304, Aéronautique DO-*)
  Non régression
  Refactoring de code en boite noire

A quel moment tester ? [ref]_
------------------------------

+---------------------------+--------------+
| Phase                     | Coût relatif |
+===========================+==============+
| Le design                 | 1            |
+---------------------------+--------------+
| L'implémentation          | 6.5          |
+---------------------------+--------------+
| Les phases de test        | 15           |
+---------------------------+--------------+
| Les phases de maintenance | 100          |
+---------------------------+--------------+

.. On notera donc qu'il faut faire les tests tôt !

Quelle quantité de tests ?
---------------------------

Exemple:

.. code-block:: cpp

    int8_t divide(int32_t a, int32_t b)
    {
        return a/b;
    }

Que doit on tester ?


Quelle quantité de tests ? (Conclusion)
-----------------------------------------

On ne peut pas tout tester -> ce sera forcément un compromis

.. Drivé principalement par les risques.


Les risques associés aux tests
------------------------------

Il faut bien considérer que les tests ont un **coût** important.
Il faut être prêt à l'assumer.

.. RetEx Bilberry.
   Ils seront d'autant plus grand que :

   - Cahier des charges trop volatile / pas de cahier des charges
   - Trop de tests mal entretenus réduisent leu utilité, on ne les exécute rapidement plus.


Les tests comme outil d'aide au développement
-----------------------------------------------

Un test permet de formaliser une spécification parfois floue.
Le Test Driven Development, et le Behavior Driven Development ont cette approche.


Techniques de test (1)
========================

Unit / Module / Integration testing
------------------------------------

- On crée des programmes de tests qui démontrent chaque point utile.
- Des niveaux d'abstraction différents.

.. Architecture / Spécification / ...

Un standard de tests unitaires : xUnit
========================================

- JUnit pour Java
- CUnit pour le C
- CppUnit pour le C++
- PyUnit pour le Python
- JsUnit pour le JavaScript
- PHPUnit pour PHP
- xunit.net pour C#

etc...

xUnit : Concepts de base [wiki_xunit]_
---------------------------------------

- Test runner (programme executant les tests)
- Test case (cas de test / élément de base de tout test)
- Test fixtures (préconditions ou contexte dans lequel le test va s'exécuter)
- Test suites (groupe de test ayant le même contexte d'execution)


.. [wiki_xunit] https://en.wikipedia.org/wiki/XUnit

xUnit : Test execution [wiki_xunit]_
-------------------------------------

L'exécution d'un test se fait comme cela :

.. code-block:: cpp

   setup(); /* First, we should prepare our 'world' to make an isolated environment for testing */
   ...
   /* Body of test - Here we make all the tests */
   ...
   teardown(); /* At the end, whether we succeed or fail, we should clean up our 'world' to
   not disturb other tests or code */

.. setup() et teardown() servent à créer un contexte. elles sont généralement les mêmes pour tous les tests d'une même suite, mais peuvent être décliné par test case.

  Il existe aussi des méthodes qui ne sont appelés qu'une fois au début de la suite, et une fois à la fin.


xUnit : Result Formatter
---------------------------------------

Permet de créer un fichier de résultats des tests dans un format standard. Par exemple qui plaira à Jenkins, ou bien directement du HTML pour l'afficher.

Paramétrable.

xUnit : Assertions
---------------------------------------

On lève une exception si une condition n'est pas remplie.
Le mot clef associé est Assertion, et la fonction utilisée pour lever cette exception est assert.

Exemples de résultats de tests
=================================

- https://ci.chromium.org/p/chromium/g/main/console
- https://test-results.appspot.com/data/layout_results/Linux_Tests__dbg__1_/108733/blink_web_tests/layout-test-results/results.html
- https://gitlab.gnome.org/GNOME/gtk/-/jobs

Atelier 1
=============

Bases sur les tests unitaires en python

.. Prenez votre temps, je pense que c'est le plus important des trois à votre niveau.

Appel
======

Atelier 1 - Correction live
============================

Conclusion de l'Atelier 1
=============================

- assert

  * Condition
  * Exception
  
- fixture
- parametrize
- mock

Techniques de test (2)
========================

Revue de code : Définition
----------------------------

Avoir un regard extérieur.
Un peu automatisé pour ce qui est du respect des standards.

.. On va regarder : les standards
   Les bugs évidents
   ...

Revue de code : Outils
---------------------------

- `Gerrit <https://www.gerritcodereview.com/>`_
- Merge request (via n'importe quelle forge) `Exemple: Gtk <https://gitlab.gnome.org/GNOME/gtk/-/merge_requests>`_
- Fichier texte ou tableur

.. forge = Github, Gitlab, ...

Revue de code : Exemple
-------------------------

`Avec Github <https://github.com/radiko-scop/Wikipedia>`_ en live.

Couverture du code
--------------------

Permet de savoir quelle partie du code a été exécuté.

.. Limité car ralenti le code -> pb avec temps réel

Atelier 2 : préparation
=============================

On repart de l'atelier 1. Il faut au minimum un test sur la librairie csv_cleaner.

Atelier 2
=============

Tests unitaires en python partie 2

Atelier 2 - Fin live
=============================

Atelier 2 - Conclusion
=========================

- Rapports standardisés
- Couverture
- Branches
- Outils utiles

Rappel des épisodes précédents
================================

Tests unitaires
-----------------

Métriques sur les tests unitaires
------------------------------------

Outils utiles
------------------------------------

Autres méthodes de tests non vue en atelier
===============================================

Theorem proving
-----------------

On prouve que le code fonctionne en écrivant la démonstration à côté (et les hypothèses associés sur les entrées/sorties).
Peut être automatisé dans une certaine mesure.

.. Je ne vais pas entrer dans le détail parce que d'une part je maitrise mal le sujet, et d'autre part ce n'est pas l'objet du cours.

   Ex : on peut facilement démonter que a/b, b étant strictement positif, va toujours fonctionner.

   **nb** : assert sur l'entrée et la sortie.

Analyse Statique
-----------------

Modélise le code pour prédire le comportement.

Exemples : `CodeSonar <https://www.grammatech.com/codesonar-cc>`_, `Cppcheck <http://cppcheck.net/>`_, `trust in soft <https://trust-in-soft.com/>`_.

.. Je ne vais pas non plus rentrer dans le détail.

Coding standards
--------------------------

Liste de règles de codage, de recommandations, et de bonnes pratiques.

Ex : PEP8, MISRA, ...

Des outils permettent de le vérifier.


Coding Standards : PEP8
------------------------

`Jetons un oeil à la spec python <https://www.python.org/dev/peps/pep-0008/>`_.

.. faire une démo live

Options de compilation pour les langages compilés
----------------------------------------------------

Les compilateurs disposent d'énormément d'options, permettant d'imposer une qualité plus ou moins stricte du code.
On peut aussi faire de l'analyse statique avec.

- `gcc <https://gcc.gnu.org/onlinedocs/gcc/Static-Analyzer-Options.html>`_
- `clang <https://clang-analyzer.llvm.org/>`_
- ``clang-tidy --list-checks``

.. -Wall en cpp par exemple

Outils généraux
------------------

`SonarQube <https://www.sonarqube.org/>`_, `Amazon Code Guru <https://aws.amazon.com/fr/codeguru/>`_, `SonarCloud <https://sonarcloud.io/summary/overall?id=microsoft_vscode-python>`_, il y en a plein...
Généralement associés à de l'automatisation -> module prochain.

Atelier 3
=============

Indicateurs de qualité en python.
Je donne le sujet sur Discord.

Atelier 3
=============

Correction live

Que faire un fois qu'on a des tests unitaires en local ?
==========================================================

Ce sera l'objet d'un prochain cours. Les mots clef sont CI/CD (intégration continue, livraison continue).

Conclusion sur les tests unitaires
==========================================================

Nous avons vu comment créer et exécuter une suite de tests , ainsi que quelques outils donnant des indicateurs sur le niveau de qualité d'un code.

**Ces outils sont transposables dans tous les langages**

Notez q'un code bien testé n'est pas un gage de qualité suffisant. Il faut encore que l'architecture et la documentation suivent.

Le plan de test
=================

Objectif : garantir que tous les moyens possibles ont été déployés pour assurer un programme de qualité.
C'est un livre qui raconte ce que vous allez tester et comment (et éventuellement pourquoi).

Personnes intervenant sur le plan de test
============================================

Le développeur n'est pas la seule personne concernée. Le plan de test se fait généralement avec un ingénieur qualité ou produit, ou une équipe dite "projet".

.. L'objectif étant de savoir ce qu'il est important de tester ou non, etc.

Le plan de test dans le cyle en V
======================================

.. image:: _static/v_cycle.svg
   :align: center

Entrées du plan de test
========================

- Normes
- Spécifications issues du

  * Cahier des charge
  * Analyse de risques
  * Technique
  * ...

Outils associés au plan de test
==================================

- Tracabilité
- Gestion documentaire

Tracabilité
-----------------

Permet de s'assurer que toutes les spécifications sont couvertes.

Tracabilité - quelques outils
------------------------------

- `Jira <https://www.atlassian.com/software/jira>`_
- `Tuleap <https://www.tuleap.org/product/tuleap-enterprise-edition-features/>`_
- `Doors <https://www.ibm.com/products/requirements-management>`_
- `Reqtify <https://www.3ds.com/products-services/catia/products/reqtify/>`_

Gestion documentaire
-----------------------

Permet de s'assurer que tous les documents sont cohérent entre eux, et ont été revus et approuvé par qui de droit.
Trop d'outils pour tous les lister ici.

Des plans de test
=====================

- Risques produit
- Budget
- Qualité attendue
- Ressources disponibles
- Procédures en place

..  Qualité : Startup veut peut être sortir vite un produit innovant. Par certains produits pour geek, ou technologiques sont plus buggés que les autres. Ex les premiers smartphone pliables. D'autre ont une image à respecter.
   - Ressources dispo : Parfois on préfèrera faire des tests manuels car les utilisateurs confirmés ne développent pas ou sont peu expérimentés, ou le planning est trop serré, ou ... A l'inverse on privilégie les tests automatiques. Dans une application médicale, il y aura sans doutes des essais cliniques en laboratoire, donc manuels, à la toute fin du process.
   - Process dans l'entreprise : Si un serveur d'intégration continue est déjà en place, on peut facilement l'utiliser.


Comment faire son (ses) plan(s) de test ?
===========================================

Test Plan : Normes
-------------------

Une norme existe à ce sujet : `ISO/IEC/IEEE 29119-3 <https://en.wikipedia.org/wiki/ISO/IEC_29119>`_

Test plan : prérequis
--------------------------

- Prendre chaque document d'entrée ou spécification
- Mettre en face de chacun un plan de test, ou faire un plan de test global
- Chaque plan de test fait référence à la spécification qu'il couvre

Test Plan : Page de garde
--------------------------

La page de garde contient :

- La version du document
- La liste des modification effectuées (optionnel si GED ?)
- La liste des références
- La liste des personnes ayant approuvé le document (optionnel si GED ?)

.. Objectif : savoir qui est en charge du document, et quels documents sont couverts.


Test Plan : Introduction
-----------------------------

Peut contenir par exemple:

- Objectif
- Vue générale
- Audience
- Définitions & acronymes

.. Le lecteur doit comprendre dans quel cadre s'inscrit le projet.

Test Plan : Stratégie (1)
-----------------------------

- Objectifs
- Hypothèses
- Principes
- Périmètre

Test Plan : Stratégie (2)
---------------------------

- Type de tests

  * Fonctionnels
  * Intégration
  * Unitaires
  * Acceptance
  * Boite noire

**non exhaustif**

.. On peut aussi citer exploratoire, système, sécurité, stress, soup, performances, IHM ...

Test Plan : Stratégie (3)
---------------------------

Pour chaque niveau de test on pourra décrire :

- Qui teste
- Quelle méthode est utilisée
- Quand les tests sont effectués
- Comment on décide que les tests passent
- Quel est le livrable attendu à la fin des tests

Test Plan : Procédures de test (1)
----------------------------------------

- environnement de test
- couverture attendue
- Quand un test est considéré comme passé

Test Plan : Procédures de test (2)
----------------------------------------

On peut aussi envisager d'écrire :

- Les métriques utilisées
- Les procédures associées aux erreurs rencontrées lors des tests

Test Plan : liste des tests
-----------------------------

Parfois on va lister les tests prévus par module.

- Facons de tester (Impérative, par design, par relecture de code, ...)
- Description du test
- Résultat attendu

.. Exemple : https://www.softwaretestinghelp.com/wp-content/qa/uploads/2014/02/Live_Project_Test_Plan_SoftwareTestingHelp.pdf
.. https://www.practitest.com/assets/pdf/system-integration-testing-template.pdf


Test Plan : Environnements
----------------------------------------

Il faut que le plan de test permette de reproduire tous les tests à l'identique. Vous devrez donc décrire l'environnement utilisé, et pourquoi pas les procédures mise en oeuvre pour conserver cet environnement.

Plan de test ne rime pas toujours avec test
===============================================

Il existe des alternatives aux test cf début de la formation.

Introduction aux Tests de charge
==================================

Fonction du temps disponible.

Bibliographie
==============

.. [ref] Source: Dawson, Maurice & Burrell, Darrell & Rahim, Emad & Brewster, Stephen. (2010). Integrating Software Assurance into the Software Development Life Cycle (SDLC). Journal of Information Systems Technology and Planning. 3. 49-53.

Inspiration (1)
===============

- Clean code (Robert Martin)
- https://github.com/topics/python?o=desc&s=stars (remplacer python par ce qu'on veut, of course)
- https://github.com/django/django/tree/main/tests
- Pour plus de détails : `wikipedia <https://en.wikipedia.org/wiki/Software_testing>`_
- https://www.tuleap.org/software-quality/tuleap-test-management-demo/
- https://www.sqlite.org/th3.html

Inspiration (2)
================

- Slides de cours magistraux:

  - http://deptinfo.unice.fr/twiki/pub/Linfo/GLEnvtProg0708/coursEnvtDeProg-l3info-0708-part5.pdf
  - https://www-pequan.lip6.fr/~vmm/fr/Enseignement/DESS/Test/Cours/C1.pdf
  - https://dept-info.labri.fr/~felix/Annee2008-09/S4/McInfo4_ASR%20Tests/1.pdf
  - Slides d'Ahmed Kenore (l'autre intervenant)


.. ::

   Mémo Syllabus:

   Partie 1 - Le Plan de Test : Acquérir une méthode d’élaboration
   - Evaluer les risques
   - Définir le niveau de qualité requis selon le projet
   - Définir les objectifs, les thèmes, les scénarios et les cas de tests
   - Construire le plan de test
   - Bonnes pratiques : du cahier des charges à la recette

   Mise en pratique : Définir le plan de test en prenant en l’architecture technique et fonctionnelle de
   l’environnement décrit dans ce cas pratique tiré d’un cas réel.
   Résultats attendus : Plan de Test – Analyse des impacts et risques liés à l’architecture technique et
   fonctionnelle existante.

   Partie 2 – Cas pratique : Réalisation de Tests
   2.1 – Tests unitaires langage C# et Visual Studio :
   • Qualité de la conception Objet
      ✓ abstraction et responsabilité
      ✓ classes abstraites
      ✓ Keep it Simple, Stupid (KISS)
      ✓ Interfaces
   • Adoption d’une interface
   • Utilisation d’un objet adoptant une interface
   • Principe et Utilisation de Mock
   • Exceptions et gestion des erreurs
      ✓ Utilisation des exceptions
      ✓ Test des exceptions

   2.2- Le langage Python et les Outils QA
   Mise en œuvre des outils de test et d’évaluation de la qualité d’un programme Python
   • Les outils d’analyse statique de code (Pylint, Pychecker)
   • Analyse  des  comptes  rendus  d’analyse  (types  de  message,  avertissements,  erreurs)
   • Le débogueur de Python (exécution pas à pas et analyse post-mortem)
   • Les modules de tests unitaires Python (Unittest...)
   • Les tests de couverture de code – profiling

   Travaux pratiques : Utilisation des outils pylint et pychecker pour la vérification d'un code Python. Mise
   en œuvre de tests unitaires.


Point sur les ateliers
========================

Sujets couverts
-----------------

Trop de sujets ? Pas assez ?

Niveau des Ateliers
------------------------------

Trop dur ? Pas Assez ?

Ratio cours / pratique
------------------------------

Trop de cours ? Pas assez ?

Pensez vous que ça va vous servir ?
--------------------------------------

Des idées pour améliorer les premiers pas ?
----------------------------------------------

L'installation et le fonctionnement de python est toujours compliqué, peut on envisager des solutions ?

Chat GPT (et google en général) ?
--------------------------------------

Autres commentaires ?
------------------------



RDV en septembre pour automatiser les concepts vus
----------------------------------------------------

- Essayez de garder votre dossier python, on s'en resservira
