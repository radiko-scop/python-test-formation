require 'rack'
require 'rack/static'

use Rack::Static, :urls => [""], :root => "_build/revealjs", :index => 'index.html'
run ->(env) {APP.call(env)}
