source .venv/bin/activate
mkdir -p pdf
labs=("labs/01-UnitTests-Part1/01-UnitTests.rst"\
	"labs/02-UnitTests-Part2/02-AdvancedUnitTests.rst"\
	"labs/03-QualityTools/03-QualityTools.rst"\
	"labs/04-LoadTests/04-LoadTests.rst")
for labPath in "${labs[@]}"
do
	echo $labPath
	filename=$(basename -- "$labPath")
	extension="${filename##*.}"
	stem="${filename%.*}"
	sed '/{{CORR/,/CORR}}/d' $labPath > pdf/${stem}_subject.rst
	rst2pdf pdf/${stem}_subject.rst -s labs/style.yaml --output=pdf/${stem}.pdf

	sed -z 's/{{CORR\|CORR}}//g' $labPath > pdf/${stem}_correction.rst
	rst2pdf pdf/${stem}_correction.rst -s labs/style.yaml --output=pdf/${stem}_correction.pdf
done
