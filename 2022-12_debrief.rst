Retour d'expérience sur cette formation
========================================

Il s'agissait d'une formation en 10h d'élèves en B3 (bac + 3)
Il n'y avait que 10 élèves, ce qui permet à peu près de les suivre en distanciel. les mettre en binôme c'est pas mal, mais ils sont nombreux à bosser en binôme chacun dans leur coin.

- Ils sont incapable de reprendre leur projet en main d'une fois sur l'autre (cours espacés de 1 mois), et reproduisent les mêmes tatônement lié à leur environnement technique -> il faudrait leur fournir soit une vm, soit un docker mais ils sont sur windows, soit des machines à distance ? En tous cas ils perdent pour la plupart un temps fou à essayer de faire marcher juste python -m pytest
- Au bout de 10h, les meilleurs auraient le temps de faire les TP 2, 3, 4 tandis que les moins bons peinent à finir le début du TP1
- Niveau donc trop disparate. Voir comment on peut s'adapter


Cours
------------

Je n'ai pas tellement essayé de déroulé le cours fait pour la première session, car à distance j'ai vraiment peu de retours, et c'est assez peu parlant.

Démo en situation:
----------------------------------

J'ai fait pas mal de live coding cette fois-ci, mais il faudrait mieux le préparer / le timer : je ne savais pas exactement combien de temps le live allait me prendre.
Peut être des live coding plus lents, orientés sur la démarche, seraient plus bénéfiques aussi : bien montrer comment on aborde le problème. Là je les ai peut être un peu largué en restant assez technique.

