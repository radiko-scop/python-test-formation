## Élèves présents

Entre 5 et 7 élèves étaient présents lors de cette session.

### 24 juillet

- Killian
- Damien
- Sydney
- Ryan
- Andy

### 25 juillet

- Andy
- Damien
- Hugo
- Delphine
- Ryan
- Sydney (Après midi seulement)
- Killian (en retard)

## Organisation

Il semble qu'on s'attendait à ce que je fasse un **cours** de 20h sur la méthodologie autour des tests unitaires. Les étudiants avaient déjà eu un atelier, de 8h,
dont 6h de cours (Atelier sur les tests unitaires).

## Installation

- < 1h. Les élèves n'ont pas accès à leur VM mais ont assez de droits pour installer des applications
- Une VM partagée serait peut être mieux, mais il les connections ssh soient bloquées vers l'extérieur.
- Windows c vraiment pourri, il faudrait faire le TP en avance sur un poste équivalent pour s'en sortir. Peut être ce serait mieux avec leur VM.

## Élèves

- Pas pressés de faire quelque chose, en même temps j'imagine que c'est la fin de l'année. Il manquait des choses dans mon TP, du coup ils n'ont rien fait pendant 15-30 min sans rien dire je pense.
- Peut être le TP part trop vite, ils ne savent pas ce que c'est un assert ?
- Globalement, le bilan de la première journée est décevant, je ne suis pas sûr qu'ils aient vraiment appris qqchse. Avec en plus des problèmes tech.
- La deuxième journée était mieux, en passant régulièrement. J'ai fait très peu de cours.
- Troisième journée : ils ont fini le TP 2, et auraient le temps de faire le 3 mais préfère glander.


## Bilan

Assez mitigé. D'un côté j'ai l'impression d'avoir +- réussi à dérouler mes TPs, d'un autre côté j'ai l'impression qu'ils n'ont pas fait grand chose, et je ne sais pas si ça va les aider / les faire progresser.
Le cours n'est pas au point par contre (en même temps c'est surtout un atelier, mais je ne suis pas à l'aise. Vu le peu d'intérêt des élèves, il faut absolument les faire participer un maximum sous peine qu'ils fassent autre chose. Surtout qu'ils ont leurs ordis pour faire autre chose).


## Bilan effectué avec les élèves.

Globalement ils ont l'air à peu près satisfait.

Découverte -> pas trop large.
Niveau difficulté : normale, des fois pas assez clair.
Par exemple pour les mock ça manquait un peu de précision.
Ratio cours / pratique -> ça allait.
Pensez vous que vous allez vous en servir -> pas tout de suite.
Premiers pas -> d'habitude VM, ça se passe mieux.

ChatGPT est utilisé, c'est amusant parce que ça semble les intéresser.

Par rapport au fait qu'ils fassent autre chose en parallèle, pas trop de réponse. Ils trouvent ça dur de rester concentrés sur 8h sur un sujet.
Plus de théorie en cours. C'est déjà bien que ce soit des TPs.
